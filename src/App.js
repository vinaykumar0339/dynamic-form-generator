import React from 'react'
import { Container, Row, Col } from 'react-bootstrap';
import { Provider } from 'react-redux';

import CustomForm from './components/customForm';
import Header from './components/Header';
import AddField from './components/AddField';
import store from './redux/store';


function App() {

    return (
      <Provider store={store} >
        <Header />
        <Container fluid className="m-auto" >
          <Row>
            <Col md={"auto"}>
              <AddField />
            </Col>
            <Col>
              <CustomForm />
            </Col>
          </Row>
        </Container>
      </Provider>
    );
}


export default App;
