import React, { Component } from 'react'
import { Form, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import uuid from 'react-uuid';
import { addField } from '../redux';


class AddField extends Component {

    constructor(props) {
        super(props)

        this.state = {
            radioCheckBox: false,
            text: true,
            number: false,
        }
    }


    selectInputType = (event) => {
        const type = event.target.value
        if (type === 'radio' || type === 'checkbox' || type === 'dropdown') {
            this.setState({
                radioCheckBox: true,
                text: false,
                number: false
            })
        } else if (type === 'text') {
            this.setState({
                text: true,
                radioCheckBox: false,
                number: false
            })
        } else if (type === 'number') {
            this.setState({
                radioCheckBox: false,
                text: false,
                number: true,
            })
        } else {
            this.setState({
                radioCheckBox: false,
                text: false,
                number: false
            })
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const elements = event.target.elements
        let field;
        let passwordConfirm;
        if (elements.type.value === 'radio'
            || elements.type.value === 'checkbox'
            || elements.type.value === 'dropdown') {
            field = {
                id: uuid(),
                type: elements.type.value,
                name: elements.name.value,
                label: elements.label.value,
                values: elements.values.value.split(','),
                required: elements.required.checked,
                error: ''
            }
        } else if (elements.type.value === 'text') {
            field = {
                id: uuid(),
                type: elements.type.value,
                name: elements.name.value,
                label: elements.label.value,
                required: elements.required.checked,
                spaceAllowed: elements['space-allow'].checked,
                error: ''
            }
        } else if (elements.type.value === 'number') {
            field = {
                id: uuid(),
                type: elements.type.value,
                name: elements.name.value,
                label: elements.label.value,
                required: elements.required.checked,
                phone: elements.phone.checked,
                error: ''
            }
        } else if (elements.type.value === 'password') {
            field = {
                id: uuid(),
                type: elements.type.value,
                name: elements.name.value,
                label: elements.label.value,
                required: elements.required.checked,
                error: ''
            }
            passwordConfirm = {
                id: uuid(),
                type: elements.type.value,
                name: elements.name.value + '_confirmation',
                label: 'Confirm ' + elements.label.value,
                required: elements.required.checked,
                error: ''
            }
        } else {
            field = {
                id: uuid(),
                type: elements.type.value,
                name: elements.name.value,
                label: elements.label.value,
                required: elements.required.checked,
                error: ''
            }
        }

        if (passwordConfirm) {
            this.props.addField([field, passwordConfirm])
        } else {
            this.props.addField([field])
        }

        this.setState({
            radioCheckBox: false,
            text: true,
            number: false
        })

        event.target.reset();
    }

    render() {
        return (
            <Form
                style={{ background: "#364547", color: "#ffe268", position: "sticky", top: 0 }}
                onSubmit={this.handleSubmit}
                className="p-3 rounded">
                <Form.Group>
                    <Form.Label>Select type of the input</Form.Label>
                    <Form.Control as="select" name="type" custom onChange={this.selectInputType}>
                        <option value="text">text</option>
                        <option value="email">email</option>
                        <option value="password">password</option>
                        <option value="number">number</option>
                        <option value="radio">radio</option>
                        <option value="checkbox">checkbox</option>
                        <option value="dropdown">dropdown</option>
                        <option value="textarea">textarea</option>
                    </Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Enter the name of the field</Form.Label>
                    <Form.Control type="text" name="name" placeholder="e.g: username, email..." />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Enter the label of the field</Form.Label>
                    <Form.Control type="text" name="label" placeholder="e.g: Username, Email..." />
                </Form.Group>
                {this.state.radioCheckBox && <Form.Group>
                    <Form.Label>Values</Form.Label>
                    <Form.Control type="text" name="values" placeholder="values separated by comma" />
                </Form.Group>}
                <Form.Group>
                    <Form.Check label="Required" inline type="checkbox" name="required" />
                    {this.state.text && <Form.Check label="Allow Space" inline type="checkbox" name="space-allow" />}
                    {this.state.number && <Form.Check label="Phone No" inline type="checkbox" name="phone" />}
                </Form.Group>
                <Form.Group>
                    <Button
                        as="input"
                        value="add"
                        type="submit"
                        block
                        style={{ background: "#2f5d62", border: "#435560", outline: "#435560" }} />
                </Form.Group>
            </Form>

        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addField: (field) => dispatch(addField(field)),
    }
}

export default connect(null, mapDispatchToProps)(AddField);