import React from 'react'
import { Navbar } from 'react-bootstrap'
import clipboard from '../asserts/icons/clipboard.png'

export default function Header() {

    return (
        <Navbar style={{background: "#2f5d62"}} className="mb-2" >
            <Navbar.Brand href="/" style={{color: "white"}} >
                <img
                    alt="app logo"
                    src={clipboard}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                />{' '}
            Generate Custom Form
            </Navbar.Brand>
        </Navbar>
    )

}
