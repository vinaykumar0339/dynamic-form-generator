import React from 'react'
import { FormGroup, FormLabel, FormCheck, FormText } from 'react-bootstrap';
import { connect } from 'react-redux';

import cancel from '../../asserts/icons/cancel.png';
import { removeField } from '../../redux';


function CheckboxRadioInput(props) {

        return (
            <FormGroup className="p-2">
                <FormLabel className="mr-2 font-weight-bold">{props.label}</FormLabel>
                <img 
                    src={cancel} 
                    alt='cancel' 
                    width="28px" 
                    height="28px" 
                    style={{ float: "right", cursor: "pointer" }} 
                    onClick={() => props.removeField(props.id)}
                    />
                {props.values.map((value, index) => {
                    return <FormCheck
                        key={index}
                        label={value}
                        inline 
                        type={props.type}
                        name={props.name}
                        value={value} />
                })}
                {props.error && <FormText className="text-danger" >{props.error}</FormText>}
            </FormGroup>
        )
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeField: (id) => dispatch(removeField(id))
    }
}

export default connect(null, mapDispatchToProps)(CheckboxRadioInput);

