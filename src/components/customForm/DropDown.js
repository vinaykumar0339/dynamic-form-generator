import React, { Component } from 'react'
import Select from 'react-select';
import { FormGroup, FormLabel, FormText } from 'react-bootstrap';
import { connect } from 'react-redux';

import cancel from '../../asserts/icons/cancel.png';
import { removeField } from '../../redux';


class DropDown extends Component {

    constructor(props) {
        super(props)

        this.state = {
            selectedValue: null
        }
    }

    options = this.props.values.map(value => {
        return {
            value: value,
            label: value.toUpperCase()[0] + value.slice(1)
        }
    })

    handleDropdownChange = (value) => {
        this.setState({
            selectedValue: value
        })
    }

    deleteField = (id) => {
        this.props.removeField(id)
    }

    render() {
        return (
            <FormGroup className="p-2">
                <FormLabel className="font-weight-bold" >{this.props.label}</FormLabel>
                <img src={cancel}
                    alt='cancel'
                    width="28px"
                    height="28px"
                    style={{ float: "right", cursor: "pointer" }}
                    onClick={() => this.props.removeField(this.props.id)} />
                <Select name={this.props.name}
                    value={this.state.selectedValue}
                    options={this.options}
                    onChange={this.handleDropdownChange} />
                {this.props.error && <FormText className="text-danger" >{this.props.error}</FormText>}
            </FormGroup>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeField: (id) => dispatch(removeField(id))
    }
}

export default connect(null, mapDispatchToProps)(DropDown)