import React from 'react'
import { FormGroup, FormLabel, FormControl, FormText } from 'react-bootstrap';
import { connect } from 'react-redux';

import cancel from '../../asserts/icons/cancel.png';
import { removeField } from '../../redux';


function Input(props) {

    return (
        <FormGroup className="p-2" >
            <FormLabel className="font-weight-bold" >{props.label}</FormLabel>
            <img
                src={cancel}
                alt='cancel'
                width="20px"
                height="20px"
                style={{ float: "right", cursor: "pointer" }}
                onClick={() => props.removeField(props.id)}
            />
            <FormControl
                type={props.type}
                name={props.name}
            />
            {props.error && <FormText className="text-danger" >{props.error}</FormText>}
        </FormGroup>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeField: (id) => dispatch(removeField(id))
    }
}

export default connect(null, mapDispatchToProps)(Input);