import React from 'react'
import { FormGroup, FormLabel, FormControl, FormText } from 'react-bootstrap';
import { connect } from 'react-redux';

import cancel from '../../asserts/icons/cancel.png';
import { removeField } from '../../redux';

function TextArea(props) {
        return (
            <FormGroup className="p-2">
                <FormLabel className="font-weight-bold" >{props.label}</FormLabel>
                <img
                    src={cancel}
                    alt='cancel'
                    width="28px"
                    height="28px"
                    style={{ float: "right", cursor: "pointer" }}
                    onClick={() => props.removeField(props.id)}
                />
                <FormControl as={props.type} rows={3} name={props.name} />
                {props.error && <FormText className="text-danger" >{props.error}</FormText>}
            </FormGroup>
        )
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeField: (id) => dispatch(removeField(id))
    }
}

export default connect(null, mapDispatchToProps)(TextArea)