import React from 'react'
import { connect } from 'react-redux';
import { Form, Button } from 'react-bootstrap';
import * as Validator from 'validatorjs';

import Input from './Input'
import CheckboxRadioInput from './CheckboxRadioInput';
import DropDown from './DropDown';
import TextArea from './TextArea';
import { updateAllFields } from '../../redux';


function CustomForm(props) {

    const handleSubmit = (event) => {
        event.preventDefault();

        let fields = Object.keys(event.target.elements)
            .filter(field => !Number.isInteger(parseInt(field)));

        let data = {}
        for (let index = 0; index < fields.length; index++) {
            data[fields[index]] = event.target.elements[fields[index]].value;
        }

        handleError(data);

    }

    const handleError = (data) => {
        let rules = getRules();

        let customErrors = {
            required: ':attribute must required',
            email: 'please enter valid :attribute',
            max: ':attribute allowed maximum 255 characters',
            alpha_num: ':attribute allow only alpha-numeric values',
            digits: 'Enter 10 digits :attribute'
        }

        let validation = new Validator(data, rules, customErrors)
        if (validation.fails()) {
            const dataFields = Object.keys(validation.errors.errors);
            const newFields = props.fields.map(field => {
                const index = dataFields.indexOf(field.name);
                const name = dataFields[index];
                if (validation.errors.errors[name]) {
                    field.error = validation.errors.errors[name][0];
                } else {
                    field.error = '';
                }

                return field
            })
            props.updateAllFields(newFields)
        } else {
            const newFields = props.fields.map(field => {
                field.error = ''
                return field
            })
            props.updateAllFields(newFields)
        }

    }

    const getRules = () => {
        let rules = {};
        props.fields.forEach(field => {
            const name = field.name;
            let ruleValue = '';
            if (field.required) {
                ruleValue += 'required|'
            }

            if (field.spaceAllowed) {
                ruleValue += 'regex:/^[a-zA-Z ]*$/|'
            }

            if (!field.spaceAllowed && field.type === 'text') {
                ruleValue += 'alpha_num|'
            }

            if (field.type === 'password' && !field.name.includes('_confirmation')) {
                ruleValue += 'confirmed|'
            }

            if (field.phone) {
                ruleValue += 'digits:10|'
            }

            if (field.type === 'email') {
                ruleValue += 'email|'
            }

            if (field.type === 'text') {
                ruleValue += 'min:1|'
            }

            if (field.type === 'textarea') {
                ruleValue += 'max:255|'
            }

            if (ruleValue.slice(-1) === '|') {
                ruleValue = ruleValue.slice(0, -1);
            }
            rules[name] = ruleValue;
        });
        return rules
    }



    return (
        <>
            {props.fields.length > 0 && <Form 
                                    onSubmit={handleSubmit} 
                                    className="p-2 rounded" 
                                    style={{ background: "#c8c6a7" }}>
                            {props.fields.map((field) => {

                                let input = null;

                                if ((field.type === 'checkbox' || field.type === 'radio')) {
                                    input = <CheckboxRadioInput
                                        key={field.id}
                                        id={field.id}
                                        type={field.type}
                                        name={field.name}
                                        values={field.values}
                                        label={field.label}
                                        error={field.error}
                                        onDelete={props.onDelete}
                                    />
                                } else if (field.type === 'dropdown') {
                                    input = <DropDown
                                        key={field.id}
                                        id={field.id}
                                        type={field.type}
                                        name={field.name}
                                        label={field.label}
                                        values={field.values}
                                        error={field.error}
                                        onDelete={props.onDelete}
                                    />
                                } else if (field.type === 'textarea') {
                                    input = <TextArea
                                        key={field.id}
                                        id={field.id}
                                        type={field.type}
                                        name={field.name}
                                        error={field.error}
                                        label={field.label}
                                        onDelete={props.onDelete}
                                    />
                                } else {
                                    input = <Input
                                        key={field.id}
                                        id={field.id}
                                        type={field.type}
                                        name={field.name}
                                        label={field.label}
                                        error={field.error}
                                        onDelete={props.onDelete}
                                    />
                                }

                                return input;

                            })}
                            <Button
                                as="input"
                                type="submit"
                                value="Submit"
                                style={{ 
                                        outline: "#6e7c7c", 
                                        cursor: "pointer", 
                                        background: "#6e7c7c", 
                                        borderColor: "#6e7c7c" }}
                                block />
            </Form>}
        </>
    )
}

const mapStateToProps = state => {
    return {
        fields: state.fields
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateAllFields: (fields) => dispatch(updateAllFields(fields)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomForm);
