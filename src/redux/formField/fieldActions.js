import { ADD_FIELD, REMOVE_FIELD, UPDATE_ALL_FIELDS } from "./fieldTypes"

export const addField = (field) => {
    return {
        type: ADD_FIELD,
        payload: {
            field
        }
    }
}

export const removeField = (id) => {
    return {
        type: REMOVE_FIELD,
        payload: {
            id
        }
    }
}

export const updateAllFields = (fields) => {
    return {
        type: UPDATE_ALL_FIELDS,
        payload: {
            fields
        }
    }
}