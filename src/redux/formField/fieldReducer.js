import { ADD_FIELD, REMOVE_FIELD, UPDATE_ALL_FIELDS } from "./fieldTypes"

const initialState = {
    fields: []
}

const fieldReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_FIELD:
            return {
                ...state,
                fields: [
                    ...state.fields,
                    ...action.payload.field
                ]
            }

        case REMOVE_FIELD:
            const id = action.payload.id;
            const newFields = state.fields.filter(field => field.id !== id);
            return {
                ...state,
                fields: newFields
            }
        
        case UPDATE_ALL_FIELDS:
            return {
                ...state,
                fields: action.payload.fields
            }

        default: return state
    }
}

export default fieldReducer;