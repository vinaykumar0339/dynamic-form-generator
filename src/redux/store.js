import { createStore } from 'redux';
import fieldReducer from './formField/fieldReducer';

const store = createStore(fieldReducer);

export default store;